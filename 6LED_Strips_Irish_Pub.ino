#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS1 37
#define NUM_LEDS2 24

// For led chips like WS2812, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define DATA_PIN1 2
#define DATA_PIN2 3
#define DATA_PIN3 4
#define DATA_PIN4 5
#define DATA_PIN5 6
#define DATA_PIN6 7

// Define the array of leds
CRGB leds1[NUM_LEDS1]; //long strip
CRGB leds2[NUM_LEDS1]; //long strip
CRGB leds3[NUM_LEDS2]; //short strip
CRGB leds4[NUM_LEDS2]; //short strip
CRGB leds5[NUM_LEDS2]; //short strip
CRGB leds6[NUM_LEDS2]; //short strip

void setup() { 
    // Uncomment/edit one of the following lines for your leds arrangement.
    // ## Clockless types ##
    FastLED.addLeds<WS2812B, DATA_PIN1, GRB>(leds1, NUM_LEDS1);  // GRB ordering is typical
    FastLED.addLeds<WS2812B, DATA_PIN2, GRB>(leds2, NUM_LEDS1);  // GRB ordering is typical
    FastLED.addLeds<WS2812B, DATA_PIN3, GRB>(leds2, NUM_LEDS2);  // GRB ordering is typical
    FastLED.addLeds<WS2812B, DATA_PIN4, GRB>(leds2, NUM_LEDS2);  // GRB ordering is typical
    FastLED.addLeds<WS2812B, DATA_PIN5, GRB>(leds2, NUM_LEDS2);  // GRB ordering is typical
    FastLED.addLeds<WS2812B, DATA_PIN6, GRB>(leds2, NUM_LEDS2);  // GRB ordering is typical
}

void loop() { 
  // Turn the LED on, then pause
  for( int i = 0; i < NUM_LEDS1; ++i) {
      leds1[i] = CRGB::Green;
  }
  /*leds[0] = CRGB::Red;
  leds[1] = CRGB::Red;
  leds[2] = CRGB::Red; */
  FastLED.show();

  for( int i = 0; i < NUM_LEDS1; ++i) {
      leds2[i] = CRGB::Green;
  }
  FastLED.show();

  for( int i = 0; i < NUM_LEDS2; ++i) {
      leds3[i] = CRGB::Green;
  }
  FastLED.show();

  for( int i = 0; i < NUM_LEDS2; ++i) {
      leds4[i] = CRGB::Green;
  }
  FastLED.show();

  for( int i = 0; i < NUM_LEDS2; ++i) {
      leds5[i] = CRGB::Green;
  }
  FastLED.show();

  for( int i = 0; i < NUM_LEDS2; ++i) {
      leds6[i] = CRGB::Green;
  }
  FastLED.show();
  
  delay(500);
  // Now turn the LED off, then pause
  /*leds[0] = CRGB::Black;
  leds[1] = CRGB::Black;
  leds[2] = CRGB::Black;*/
/*
  for( int i = 0; i < NUM_LEDS; ++i) {
      leds[i] = CRGB::Black;
  }    
  FastLED.show();

  for( int i = 0; i < NUM_LEDS; ++i) {
      leds1[i] = CRGB::Black;
  }    
  FastLED.show();

  delay(500);*/
}
